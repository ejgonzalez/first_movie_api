import os
from slqalchemy import create_engine

sqlite_file_name="database.sqlite"
# lee el directorio
base_dir=os.path.dirname(os.path.realpath(__file__))
# se crea una direccion dinámicamente a la base de datos
database_url=f"sqlite:///{os.path.join(base_dir,sqlite_file_name)}"

engine=