from starlette.middleware.base import BaseHTTPMiddleware
from fastapi import FastAPI, Request

class ErrorHandler(BaseHTTPMiddleware):
    #constructor
    def __init__(self, app: fastAPI) -> None:
        super().__init__(app)
    
    #este método es el que se ejcuta cuando ocurre un error
    async def dispacth(self, request: Request, call_next)->Response:
        return await super().dispacth(request, call_next)