from models.movie import Movie as MovieModel
class MovieService():
    # metodo constructor que requerira una instancia a la Base de Datos
    def __init__(self,db) -> None:
        self.db = db

    # metodo para consultar todas las peliculas usando un servicio
    def get_movies(self):
        result= self.db.query(MovieModel).all()
        return result
    
    #metodo para consultar por Id
    def get_movie(self, id):
        result= self.db.query(MovieModel).filter(MovieModel.id==id).first()
        return result    