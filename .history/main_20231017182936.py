from fastapi import FastAPI
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel,Field

from jwt_managr import create_token,validate_token
from config.database import engine, Base
#importamos el routers
from routers.movie import movie_router

from middleware.error_handler import ErrorHandler

movies=[
    {'id':1,
     'title':'Avatar',
     'overview':'En un lejano planeta',
     'year':2009,
     'rating':'7.8',
    'category':'Accion'
     },
    {'id':2,
     'title':'Avatar 2',
     'overview':'El camino del Agua',
     'year':2022,
     'rating':'9',
    'category':'Suspenso'
     }     
]

app = FastAPI()
app.title='Prueba de FastAPI Python'
app.version='1.0.0'
app.add_middleware(ErrorHandler)
app.include_router(movie_router)


Base.metadata.create_all(bind=engine)

'''class JWTBearer(HTTPBearer):
    # esto se agrega por que es una funcion asincrona que tiene un largo proceso de espera
    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data= validate_token (auth.credentials)
        if data['email']!='eudo@gmail.com':
            raise HTTPException(status_code=403,detail="Credenciales invalidas")'''

class User(BaseModel):
    email : str
    password : str


@app.get('/',tags=['home'])
def message():
    return HTMLResponse("<h4>Hola</h4>")


# metodo que logea compara el email y la clave enviadas desde  el formulario
# se crea el token si la clave y el usuario coinciden
@app.post("/login", tags=["auth"])
def login(user: User):
    if (user.email=="eudo@gmail.com") and (user.password=='12345'):
        token: str = create_token(user.dict())
        return JSONResponse (status_code=202,content={"token":token})
    
    return JSONResponse (status_code=401,content={"message":"No autorizado"})


