from fastapi import FastAPI, Body, Path,Query, Request, HTTPException, Depends
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel,Field
from typing import Any, Coroutine, Optional, List

from starlette.requests import Request
from jwt_managr import create_token,validate_token
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from config.database import Session, engine, Base
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder


movies=[
    {'id':1,
     'title':'Avatar',
     'overview':'En un lejano planeta',
     'year':2009,
     'rating':'7.8',
    'category':'Accion'
     },
    {'id':2,
     'title':'Avatar 2',
     'overview':'El camino del Agua',
     'year':2022,
     'rating':'9',
    'category':'Suspenso'
     }     
]



app = FastAPI()
app.title='Prueba de FastAPI Python'
app.version='1.0.0'

Base.metadata.create_all(bind=engine)

class JWTBearer(HTTPBearer):
    # esto se agrega por que es una funcion asincrona que tiene un largo proceso de espera
    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data= validate_token (auth.credentials)
        if data['email']!='eudo@gmail.com':
            raise HTTPException(status_code=403,detail="Credenciales invalidas")

class User(BaseModel):
    email : str
    password : str

class Movie(BaseModel):
    id : Optional[int] = None
    title : str = Field( min_length=5, max_length=15) 
    overview : str     
    year : int = Field(ge=1970,le=2022)
    rating : float =Field(ge=1,le=10)
    category : str = Field( min_length=5, max_length=25) 

    model_config = {
        "json_schema_extra": {
                "examples": [
                    {
                        "id": 1,
                        "title": "Mi Pelicula",
                        "overview": "Descripcion de la pelicula",
                        "year": 2022,
                        "rating": 9.9,
                        "category": "Acción"
                    }
                ]
            }
    }

@app.get('/',tags=['home'])
def message():
    return HTMLResponse("<h4>Hola</h4>")

@app.get('/movies',tags=['movies'],response_model=List[Movie],status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies()->list[Movie]:
    db = Session()
    result=db.query(MovieModel).all()
    return JSONResponse(content=result)

# esto es la busqueda por id de la pelicula
@app.get('/movies/{id}',tags=['movies'], response_model=Movie)
def get_movie(id:int = Path(ge=1, le=2000)):
    new_list = list(filter(lambda item: item['id'] == id, movies))
    '''for item in movies:
        if item['id']==str(id):
            return item '''
    return JSONResponse(status_code=200, content=new_list)

#esto es la busqueda por categoria
#en este caso el parametro se coloca en la función, no en la ruta
# es un parámetro query
# cada parametro debe ser un elemento en la función
@app.get('/movies/',tags=['movies'],response_model=List[Movie])
def get_movies_by_category(category:str = Query(min_length=5,max_length=50)):
    data = [ item for item in movies if item['category']== category]
    '''    for item in movies:
        if item['category']==category:
            return item '''
    return JSONResponse(status_code=200,content=data)

# esta funcion permite crear una pelicula
# se añade a cada parametro el tag body para que no lo tome como un parametro query sino como parte 
# de la peticion 
@app.post ('/movies/',tags=['movies'], response_model=dict)
#def create_movies(id:int = Body(), title:str = Body() , overview:str = Body(), year:int = Body(), rating: float = Body(),category:str = Body()):
def create_movies(movie : Movie)->dict:
    db=Session()
    # hay dos formas de ingresar la data la primera es 
    # primera forma
    #MovieModel(title=movie.title, year=movie.year, rating=movie.rating, category=movie.category)
    #Segunda Forma
    new_movie=MovieModel(**movie.dict())
    #creamos el nuevo registro en la tabla
    db.add(new_movie)
    #confirmamos los cambios
    db.commit()
    #movies.append(movie)
    #return movies[-1]
    return JSONResponse(status_code=201,content={"message":"Success"})


@app.put('/movies/{id}',tags=['movies'], response_model=dict)
#def update_movie(id:int,title:str = Body() , overview:str = Body(), year:int = Body(), rating: float = Body(),category:str = Body()):
def update_movie(id:int, movie: Movie)->dict:
    for item in movies:
        if item['id']==id:
            item['title']=movie.title     
            item['overview']=movie.overview
            item['year']=movie.year
            item['rating']=movie.rating
            item['category']=movie.category

            #return item
            return JSONResponse(content={"message":"Success"})
        
    return JSONResponse(status_code=404,content={"message":"Error, don't find the movie"})

@app.delete ('/movies/{id}',tags=['movies'], response_model=dict)
def delete_movie(id:int)->dict:
    for item in movies:
        if item['id']==id:
            movies.remove(item)
            return JSONResponse(content={"message":"Success"})

    return JSONResponse(status_code=404,content={"message":"Error, don't deleted the movie"})

@app.post("/login", tags=["auth"])
def login(user: User):
    if (user.email=="eudo@gmail.com") and (user.password=='12345'):
        token: str = create_token(user.dict())
        return JSONResponse (status_code=202,content={"token":token})
    
    return JSONResponse (status_code=401,content={"message":"No autorizado"})


