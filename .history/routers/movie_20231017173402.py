from fastapi import APIRouter
from fastapi import FastAPI, Body, Path,Query, Request, HTTPException, Depends
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel,Field
from typing import Any, Coroutine, Optional, List

from starlette.requests import Request
from jwt_managr import create_token,validate_token
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from config.database import Session, engine, Base
from models.movie import Movie as MovieModel
# dependencia que coinvierte los objketos tipo Bd a json
from fastapi.encoders import jsonable_encoder
#importamos el routers
#from routers.movie import movie_router

from middleware.error_handler import ErrorHandler
from middleware.jwt_bearer import JWTBearer
movie_router = APIRouter()

class Movie(BaseModel):
    id : Optional[int] = None
    title : str = Field( min_length=5, max_length=15) 
    overview : str     
    year : int = Field(ge=1970,le=2022)
    rating : float =Field(ge=1,le=10)
    category : str = Field( min_length=5, max_length=25) 

    model_config = {
        "json_schema_extra": {
                "examples": [
                    {
                        "id": 1,
                        "title": "Mi Pelicula",
                        "overview": "Descripcion de la pelicula",
                        "year": 2022,
                        "rating": 9.9,
                        "category": "Acción"
                    }
                ]
            }
    }

@movie_router.get('/movies',tags=['movies'],response_model=List[Movie],status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies()->list[Movie]:
    db = Session()
    result=db.query(MovieModel).all()
    # debemnos convertir los objetos tipo BD a Json
    return JSONResponse(content=jsonable_encoder(result))

# esto es la busqueda por id de la pelicula
@movie_router.get('/movies/{id}',tags=['movies'], response_model=Movie)
def get_movie(id:int = Path(ge=1, le=2000)):
    db = Session()
    #result = db.query(MovieModel).filter(MovieModel.id==id).first()
    result = db.query(MovieModel).filter(MovieModel.id==id).first()
    if not result:
        return  JSONResponse(status_code=404, content={"message":"No encontrado"})
    #new_list = list(filter(lambda item: item['id'] == id, movies))
    '''for item in movies:
        if item['id']==str(id):
            return item '''
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#esto es la busqueda por categoria
#en este caso el parametro se coloca en la función, no en la ruta
# es un parámetro query
# cada parametro debe ser un elemento en la función
@movie_router.get('/movies/',tags=['movies'],response_model=List[Movie])
def get_movies_by_category(category:str = Query(min_length=5,max_length=50)):
    db = Session()
    result = db.query(MovieModel).filter(MovieModel.category==category).all()
    if not result:
        return  JSONResponse(status_code=404, content={"message":"No encontrado"})
    #data = [ item for item in movies if item['category']== category]
    '''    for item in movies:
        if item['category']==category:
            return item '''
    return JSONResponse(status_code=200,content=jsonable_encoder(result))

# esta funcion permite crear una pelicula
# se añade a cada parametro el tag body para que no lo tome como un parametro query sino como parte 
# de la peticion 
@movie_router.post ('/movies/',tags=['movies'], response_model=dict)
#def create_movies(id:int = Body(), title:str = Body() , overview:str = Body(), year:int = Body(), rating: float = Body(),category:str = Body()):
def create_movies(movie : Movie)->dict:
    db=Session()
    # hay dos formas de ingresar la data la primera es 
    # primera forma
    #MovieModel(title=movie.title, year=movie.year, rating=movie.rating, category=movie.category)
    #Segunda Forma
    new_movie=MovieModel(**movie.dict())
    #creamos el nuevo registro en la tabla
    db.add(new_movie)
    #confirmamos los cambios
    db.commit()
    #movies.movie_routerend(movie)
    #return movies[-1]
    return JSONResponse(status_code=201,content={"message":"Success"})


@movie_router.put('/movies/{id}',tags=['movies'], response_model=dict)
#def update_movie(id:int,title:str = Body() , overview:str = Body(), year:int = Body(), rating: float = Body(),category:str = Body()):
def update_movie(id:int, movie: Movie)->dict:
    db = Session()
    result=db.query(MovieModel).filter(MovieModel.id==id).first()
    if not result:
        return  JSONResponse(status_code=404, content={"message":"No encontrado"})

    result.title=movie.title
    result.overview=movie.overview
    result.year=movie.year
    result.rating=movie.rating
    result.category=movie.category
    
    #conformamos los cambios
    db.commit()

    '''    for item in movies:
    if item['id']==id:
        item['title']=movie.title     
        item['overview']=movie.overview
        item['year']=movie.year
        item['rating']=movie.rating
        item['category']=movie.category

        #return item'''
    return JSONResponse(content={"message":"Success"})

@movie_router.delete ('/movies/{id}',tags=['movies'], response_model=dict)
def delete_movie(id:int)->dict:
    db = Session()
    result=db.query(MovieModel).filter(MovieModel.id==id).first()
    if not result:
        return  JSONResponse(status_code=404, content={"message":"No encontrado"})    
        '''    for item in movies:
        if item['id']==id:
            movies.remove(item)
            return JSONResponse(content={"message":"Success"})'''
    db.delete(result)
    db.commit()
    return JSONResponse(status_code=200,content={"message":"Se eliminó de forma exitosa"})