from fastapi import FastAPI, Body, Path,Query
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel,Field
from typing import Optional, List
from jwt_managr import create_token


movies=[
    {'id':1,
     'title':'Avatar',
     'overview':'En un lejano planeta',
     'year':2009,
     'rating':'7.8',
    'category':'Accion'
     },
    {'id':2,
     'title':'Avatar 2',
     'overview':'El camino del Agua',
     'year':2022,
     'rating':'9',
    'category':'Suspenso'
     }     
]

app = FastAPI()
app.title='Prueba de FastAPI Python'
app.version='1.0.0'

class User(BaseModel):
    email : str
    password : str

class Movie(BaseModel):
    id : Optional[int] = None
    title : str = Field( min_length=5, max_length=15) 
    overview : str     
    year : int = Field(ge=1970,le=2022)
    rating : float =Field(ge=1,le=10)
    category : str = Field( min_length=5, max_length=25) 

    model_config = {
        "json_schema_extra": {
                "examples": [
                    {
                        "id": 1,
                        "title": "Mi Pelicula",
                        "overview": "Descripcion de la pelicula",
                        "year": 2022,
                        "rating": 9.9,
                        "category": "Acción"
                    }
                ]
            }
    }

@app.get('/',tags=['home'])
def message():
    return HTMLResponse("<h4>Hola</h4>")

@app.get('/movies',tags=['movies'],response_model=List[Movie],status_code=200)
def get_movies()->list[Movie]:
    return JSONResponse(content=movies)

# esto es la busqueda por id de la pelicula
@app.get('/movies/{id}',tags=['movies'], response_model=Movie)
def get_movie(id:int = Path(ge=1, le=2000)):
    new_list = list(filter(lambda item: item['id'] == id, movies))
    '''for item in movies:
        if item['id']==str(id):
            return item '''
    return JSONResponse(status_code=200, content=new_list)

#esto es la busqueda por categoria
#en este caso el parametro se coloca en la función, no en la ruta
# es un parámetro query
# cada parametro debe ser un elemento en la función
@app.get('/movies/',tags=['movies'],response_model=List[Movie])
def get_movies_by_category(category:str = Query(min_length=5,max_length=50)):
    data = [ item for item in movies if item['category']== category]
    '''    for item in movies:
        if item['category']==category:
            return item '''
    return JSONResponse(status_code=200,content=data)

# esta funcion permite crear una pelicula
# se añade a cada parametro el tag body para que no lo tome como un parametro query sino como parte 
# de la peticion 
@app.post ('/movies/',tags=['movies'], response_model=dict)
#def create_movies(id:int = Body(), title:str = Body() , overview:str = Body(), year:int = Body(), rating: float = Body(),category:str = Body()):
def create_movies(movie : Movie)->dict:
    '''    movies.append({
        'id':id,
        'title':title,
        'overview':overview,
        'year':year,
        'rating':rating,
        'category':category
    })'''
    movies.append(movie)
    #return movies[-1]
    return JSONResponse(status_code=201,content={"message":"Success"})


@app.put('/movies/{id}',tags=['movies'], response_model=dict)
#def update_movie(id:int,title:str = Body() , overview:str = Body(), year:int = Body(), rating: float = Body(),category:str = Body()):
def update_movie(id:int, movie: Movie)->dict:
    for item in movies:
        if item['id']==id:
            item['title']=movie.title     
            item['overview']=movie.overview
            item['year']=movie.year
            item['rating']=movie.rating
            item['category']=movie.category

            #return item
            return JSONResponse(content={"message":"Success"})
        
    return JSONResponse(status_code=404,content={"message":"Error, don't find the movie"})

@app.delete ('/movies/{id}',tags=['movies'], response_model=dict)
def delete_movie(id:int)->dict:
    for item in movies:
        if item['id']==id:
            movies.remove(item)
            return JSONResponse(content={"message":"Success"})

    return JSONResponse(status_code=404,content={"message":"Error, don't deleted the movie"})

@app.post("/login", tags=["auth"])
def login(User):
    return User