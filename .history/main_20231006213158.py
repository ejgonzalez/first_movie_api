from fastapi import FastAPI, Body
from fastapi.responses import HTMLResponse

movies=[
    {'id':1,
     'title':'Avatar',
     'overview':'En un lejano planeta',
     'year':2009,
     'rating':'7.8',
    'category':'Accion'
     },
    {'id':2,
     'title':'Avatar 2',
     'overview':'El camino del Agua',
     'year':2022,
     'rating':'9',
    'category':'Suspenso'
     }     
]

app = FastAPI()
app.title='Prueba de FastAPI Python'
app.version='1.0.0'

@app.get('/',tags=['home'])
def message():
    return HTMLResponse("<h4>Hola</h4>")

@app.get('/movies',tags=['movies'])
def get_movies():
    return movies

# esto es la busqueda por id de la pelicula
@app.get('/movies/{id}',tags=['movies'])
def get_movie(id:int):
    new_list = list(filter(lambda item: item['id'] == str(id), movies))
    '''for item in movies:
        if item['id']==str(id):
            return item '''
    return new_list

#esto es la busqueda por categoria
#en este caso el parametro se coloca en la función, no en la ruta
# es un parámetro query
# cada parametro debe ser un elemento en la función
@app.get('/movies/',tags=['movies'])
def get_movies_by_category(category:str):
    for item in movies:
        if item['category']==category:
            return item 

    return []

# esta funcion permite crear una pelicula
# se añade a cada parametro el tag body para que no lo tome como un parametro query sino como parte 
# de la peticion 
@app.post ('/movies/',tags=['movies'])
def create_movies(id:int = Body(), title:str = Body() , overview:str = Body(), year:int = Body(), rating: float = Body(),category:str = Body()):
    movies.append({
        'id':id,
        'title':title,
        'overview':overview,
        'year':year,
        'rating':rating,
        'category':category
    })
    return movies[-1]


@app.put('/movies/{id}',tags=['movies'])
def update_movie(id:int,title:str = Body() , overview:str = Body(), year:int = Body(), rating: float = Body(),category:str = Body()):
    for item in movies:
        if item['id']==id:
            item['title']=title     
            item['overview']=overview
            item['year']=year
            item['rating']=rating
            item['category']=category

            return item
        
    return []

@app.delete ('/movies/{id}',tags=['movies'])
def delete_movie(id:int):
    for item in movies:
        if item['id']==id:
            movies.remove(item)

            return 'Success'
    return "No encontrado"

